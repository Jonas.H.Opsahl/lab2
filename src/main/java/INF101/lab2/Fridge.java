package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    private ArrayList <FridgeItem> Fridge = new ArrayList<FridgeItem>();
    private final int FridgeMaximum = 20;

    
    
    @Override
    public int nItemsInFridge() {
        return this.Fridge.size();
    }

    @Override
    public int totalSize() {
        return FridgeMaximum;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        int number_of_items = Fridge.size();
        if (number_of_items == FridgeMaximum) {
            return false;

        }
        else {
            Fridge.add(item);
            return true;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (Fridge.contains(item)) {
            Fridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        Fridge.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        for (FridgeItem item : Fridge) {
            if (item.hasExpired()) {
                expiredItems.add(item);
                //Fridge.remove(item);
            }
        }
        for (FridgeItem item : expiredItems) {
            Fridge.remove(item);
        }
        return expiredItems;
    }
    
}
